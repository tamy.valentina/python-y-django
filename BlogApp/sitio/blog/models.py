from django.db import models
from django.utils import timezone

# Create your models here.
class Post(models.Model):
    autor = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    titulo = models.CharField("Titulo", max_length=200)
    articulo = models.TextField("articulo")
    fecha_creacion = models.DateField("Fechada creacion", default=timezone.now)
    fecha_publicacion = models.DateField("Fecha Publicacion", blank=True, null=True)

    def publish(self):
        self.fecha_publicacion = timezone.now()
        self.save()

    def _str_(self):
        return self.titulo